library ieee;
use ieee. std_logic_1164.all;

entity D_FF is
     port( 
          D    : in std_logic;
          CLK  : in std_logic;
          Q    : out std_logic
     );
end D_FF;

architecture behavioral of D_FF is
begin
     process(CLK)
     begin
          if(CLK='0' and CLK'EVENT) then
               Q <= D;
          end if;
     end process;
end behavioral;
