library ieee;
use ieee.std_logic_1164.all;

entity ALU_TB is 
end ALU_TB;

architecture behavioral of ALU_TB is
     
     component ALU is
     port( 
          e  : in std_logic;
          A  : in  std_logic_vector(7 downto 0);
          B  : in  std_logic_vector(7 downto 0);
          F  : in  std_logic_vector(3 downto 0);
          C  : out std_logic_vector(7 downto 0);
          q  : out std_logic_vector(3 downto 0)
     );
     end component;
     signal e  : std_logic;
     signal A  : std_logic_vector(7 downto 0);
     signal B  : std_logic_vector(7 downto 0);
     signal F  : std_logic_vector(3 downto 0);
     signal C  : std_logic_vector(7 downto 0);
     signal q  : std_logic_vector(3 downto 0);

begin 
     uut: ALU port map(e,A,B,F,C,q);
     
     stim_proc: process
     begin
	     wait for 1 ns;
          e<='1';
          A<=x"A5";
          B<=x"5A";
          F<=x"0";
          wait for 20 ns;
          e<='0';
          wait for 20 ns;
          e<='1';
          wait for 20 ns;
          F<=x"1";
          e<='0';
	     wait for 20 ns;
          e<='1';
	     wait for 20 ns;
          F<=x"2";
          e<='0';
	     wait for 20 ns;
          e<='1';
	     wait for 20 ns;
          F<=x"3";
          e<='0';
	     wait for 20 ns;
          e<='1';
	     wait for 20 ns;
          F<=x"4";
          e<='0';
	     wait for 20 ns;
          e<='1';
          wait for 20 ns;
          F<=x"5";
          e<='0';
	     wait for 20 ns;
          e<='1';
	     wait for 20 ns;
          F<=x"6";
          e<='0';
	     wait for 20 ns;
          e<='1';
	     wait for 20 ns;
          F<=x"7";
          e<='0';
	     wait for 20 ns;
          e<='1';
	     wait for 20 ns;
          F<=x"8";
          e<='0';
	     wait for 20 ns;
          e<='1';
	     wait for 20 ns;
          F<=x"9";
          e<='0';
	     wait for 20 ns;
          e<='1';
	     wait for 20 ns;
          F<=x"A";
          e<='0';
	     wait for 20 ns;
          e<='1';
	     wait for 20 ns;
          F<=x"B";
          e<='0';
	     wait for 20 ns;
          e<='1';
	     wait for 20 ns;
	     wait;
     end process;
end behavioral; 
