library ieee;
use ieee.std_logic_1164.all;

entity REG_FL is 
     port( 
          rs1_a: in  std_logic_vector(3 downto 0);
          rs2_a: in  std_logic_vector(3 downto 0);
          rd_a : in  std_logic_vector(3 downto 0);
          rw   : in  std_logic;
          rs1_e: in  std_logic;
          rs2_e: in  std_logic;
          DEST : in  std_logic_vector(7 downto 0);
          S1   : out std_logic_vector(7 downto 0);
          S2   : out std_logic_vector(7 downto 0)
     ); 
end REG_FL;

architecture behavioral of REG_FL is
     
     component REG_8 is
               port(
                    DEST : in  std_logic_vector(7 downto 0);
                    S1   : out std_logic_vector(7 downto 0);
                    S2   : out std_logic_vector(7 downto 0);
                    rdN  : in  std_logic;
                    rs1N : in  std_logic;
                    rs2N : in  std_logic
               ); 
     end component;
     
     component REG_ZERO is
               port(
                    DEST : in  std_logic_vector(7 downto 0);
                    S1   : out std_logic_vector(7 downto 0);
                    S2   : out std_logic_vector(7 downto 0);
                    rdN  : in  std_logic;
                    rs1N : in  std_logic;
                    rs2N : in  std_logic
               ); 
     end component;
     
     component DEMUX is
          port(
               enb  :in  std_logic;
               S    :in  std_logic_vector(3 downto 0);
               O    :out std_logic_vector(15 downto 0)
          ); 
     end component;
     
     component BUFF_8 is
          port(
               input     :in  std_logic_vector(7 downto 0);
               output    :out std_logic_vector(7 downto 0);
               enable    :in  std_logic
          );
     end component;

     signal S1_t :std_logic_vector(7 downto 0);
     signal S2_t :std_logic_vector(7 downto 0);
     signal rs1N :std_logic_vector(15 downto 0):=x"FFFF";
     signal rs2N :std_logic_vector(15 downto 0):=x"FFFF";
     signal rdN  :std_logic_vector(15 downto 0);
     signal r_enb:std_logic:='1';   
begin 
     rs1a:  DEMUX  port map(rs1_e,rs1_a(3 downto 0),rs1N);  
     rs2a:  DEMUX  port map(rs2_e,rs2_a(3 downto 0),rs2N);  
     rda :  DEMUX  port map(rw,rd_a(3 downto 0),rdN);
     S1_O:  BUFF_8 port map(S1_t, S1, rs1_e);
     S2_O:  BUFF_8 port map(S2_t, S2, rs2_e);
     R0F :  REG_8  port map(DEST,S1_t,S2_t,rdN(15), rs1N(15), rs2N(15));
     R0E :  REG_8  port map(DEST,S1_t,S2_t,rdN(14), rs1N(14), rs2N(14));
     R0D :  REG_8  port map(DEST,S1_t,S2_t,rdN(13), rs1N(13), rs2N(13));
     R0C :  REG_8  port map(DEST,S1_t,S2_t,rdN(12), rs1N(12), rs2N(12));
     R0B :  REG_8  port map(DEST,S1_t,S2_t,rdN(11), rs1N(11), rs2N(11));
     R0A :  REG_8  port map(DEST,S1_t,S2_t,rdN(10), rs1N(10), rs2N(10));
     R09 :  REG_8  port map(DEST,S1_t,S2_t,rdN(9), rs1N(9), rs2N(9));
     R08 :  REG_8  port map(DEST,S1_t,S2_t,rdN(8), rs1N(8), rs2N(8));
     R07 :  REG_8  port map(DEST,S1_t,S2_t,rdN(7), rs1N(7), rs2N(7));
     R06 :  REG_8  port map(DEST,S1_t,S2_t,rdN(6), rs1N(6), rs2N(6));
     R05 :  REG_8  port map(DEST,S1_t,S2_t,rdN(5), rs1N(5), rs2N(5));
     R04 :  REG_8  port map(DEST,S1_t,S2_t,rdN(4), rs1N(4), rs2N(4));
     R03 :  REG_8  port map(DEST,S1_t,S2_t,rdN(3), rs1N(3), rs2N(3));
     R02 :  REG_8  port map(DEST,S1_t,S2_t,rdN(2), rs1N(2), rs2N(2));
     R01 :  REG_8  port map(DEST,S1_t,S2_t,rdN(1), rs1N(1), rs2N(1));
     R00 :  REG_ZERO  port map(DEST,S1_t,S2_t,rdN(0), rs1N(0), rs2N(0));

end behavioral; 
