library ieee;
use ieee.std_logic_1164.all;

entity REG_ZERO is 
     port(  
          DEST : in  std_logic_vector(7 downto 0);
          S1   : out std_logic_vector(7 downto 0);
          S2   : out std_logic_vector(7 downto 0);
          rdN  : in  std_logic;
          rs1N : in  std_logic;
          rs2N : in  std_logic
     ); 
end REG_ZERO;

architecture behavioral of REG_ZERO is
begin
     --FF to S1 bus
     process(rdN,rs1N)
     begin
          if(rs1N='0') then
               S1<=x"00";
          else
               S1<="ZZZZZZZZ";
          end if;
     end process;
     
     --FF to S2 bus
     process(rdN,rs2N)
     begin
          if(rs2N='0') then
               S2<=x"00";
          else
               S2<="ZZZZZZZZ";
          end if;
     end process;
end behavioral; 
