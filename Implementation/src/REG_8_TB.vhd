library ieee;
use ieee.std_logic_1164.all;

entity REG_8_TB is 
end REG_8_TB;

architecture behavioral of REG_8_TB is
     
     component REG_8 is
	  port(  
	       DEST : in  std_logic_vector(7 downto 0);
	       S1   : out std_logic_vector(7 downto 0);
	       S2   : out std_logic_vector(7 downto 0);
	       rdN  : in  std_logic;
	       rs1N : in  std_logic;
	       rs2N : in  std_logic
	  ); 
     end component;
     signal DEST : std_logic_vector(7 downto 0);
     signal S1   : std_logic_vector(7 downto 0);
     signal S2   : std_logic_vector(7 downto 0);
     signal rdN  : std_logic;
     signal rs1N : std_logic;
     signal rs2N : std_logic;

begin 
     uut: REG_8 port map(DEST,S1,S2,rdN,rs1N,rs2N);
     
     stim_proc: process
     begin
	  wait for 20 ns;
	  DEST<= x"00";
	  rdN <= '1';
	  rs1N<= '1';
	  rs2N<= '1';
	  wait for 20 ns;
	  DEST<= x"FF";
	  wait for 20 ns;
	  rdN  <= '0';
	  rs1N<= '1';
	  rs2N<= '1';
	  wait for 20 ns;
	  DEST<= x"0A";
	  rdN <= '1';
	  rs1N<= '1';
	  rs2N<= '0';
	  wait for 20 ns;
	  DEST<= x"EE";
	  rdN <= '0';
	  wait for 20 ns;
	  DEST<= x"0A";
	  rdN <= '0';
	  rs1N<= '0';
	  rs2N<= '1';
	  wait for 20 ns;
	  wait;
     end process;
end behavioral; 
