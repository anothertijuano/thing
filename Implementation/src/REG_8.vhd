library ieee;
use ieee.std_logic_1164.all;

entity REG_8 is 
     port(  
          DEST : in  std_logic_vector(7 downto 0);
          S1   : out std_logic_vector(7 downto 0);
          S2   : out std_logic_vector(7 downto 0);
          rdN  : in  std_logic;
          rs1N : in  std_logic;
          rs2N : in  std_logic
     ); 
end REG_8;

architecture behavioral of REG_8 is
     
     component TRI_BUFF is
          port(
               input     : in  std_logic;
               output    : out std_logic;
               enable    : in  std_logic
          );
     end component;
     
     component D_FF is
          port(
               D    : in std_logic;
               CLK  : in std_logic;
               Q    : out std_logic
          );
     end component;

     --Output of FlipFlops and input of Buffers
     signal Q  : std_logic_vector(7 downto 0):=x"00";
begin 
     --DEST bus to FF
     Q_07 : D_FF port map(DEST(7),rdn,Q(7));
     Q_06 : D_FF port map(DEST(6),rdn,Q(6));
     Q_05 : D_FF port map(DEST(5),rdn,Q(5));
     Q_04 : D_FF port map(DEST(4),rdn,Q(4));
     Q_03 : D_FF port map(DEST(3),rdn,Q(3));
     Q_02 : D_FF port map(DEST(2),rdn,Q(2));
     Q_01 : D_FF port map(DEST(1),rdn,Q(1));
     Q_00 : D_FF port map(DEST(0),rdn,Q(0));
     
     --FF to S1 bus
     S1_07: TRI_BUFF port map(Q(7),S1(7),rs1N);
     S1_06: TRI_BUFF port map(Q(6),S1(6),rs1N);
     S1_05: TRI_BUFF port map(Q(5),S1(5),rs1N);
     S1_04: TRI_BUFF port map(Q(4),S1(4),rs1N);
     S1_03: TRI_BUFF port map(Q(3),S1(3),rs1N);
     S1_02: TRI_BUFF port map(Q(2),S1(2),rs1N);
     S1_01: TRI_BUFF port map(Q(1),S1(1),rs1N);
     S1_00: TRI_BUFF port map(Q(0),S1(0),rs1N);
     
     --FF to S2 bus
     S2_07: TRI_BUFF port map(Q(7),S2(7),rs2N);
     S2_06: TRI_BUFF port map(Q(6),S2(6),rs2N);
     S2_05: TRI_BUFF port map(Q(5),S2(5),rs2N);
     S2_04: TRI_BUFF port map(Q(4),S2(4),rs2N);
     S2_03: TRI_BUFF port map(Q(3),S2(3),rs2N);
     S2_02: TRI_BUFF port map(Q(2),S2(2),rs2N);
     S2_01: TRI_BUFF port map(Q(1),S2(1),rs2N);
     S2_00: TRI_BUFF port map(Q(0),S2(0),rs2N);

end behavioral; 
