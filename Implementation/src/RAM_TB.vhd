library ieee;
use ieee.std_logic_1164.all;
entity RAM_TB is 
end RAM_TB;

architecture behavioral of RAM_TB is

     component RAM is 
     port( 
       cs : in std_logic;
       we : in std_logic;
       addr : in std_logic_vector(7 downto 0);
       data : inout std_logic_vector(15 downto 0)
     ); 
     end component;

     signal cs : std_logic;
     signal we : std_logic;
     signal addr : std_logic_vector(7 downto 0);
     signal data : std_logic_vector(15 downto 0);

begin
  
     uut: RAM port map(cs, we, addr, data);
     
     stim_proc: process
     begin
       wait for 1 ns;
       cs <= '0';
       we <= '0';
       addr <= x"ab";
       data <= x"abcd";
       wait for 20 ns;
       cs <= '0';
       we <= '1';
       addr <= x"ab";
       data <= x"abcd";
       wait for 20 ns;
       cs <= '1';
       we <= '1';
       addr <= x"ab";
       data <= x"abcd";
       wait for 20 ns;
       cs <= '1';
       we <= '1';
       addr <= x"ae";
       data <= x"abed";
       wait for 20 ns;
       cs <= '1';
       we <= '0';
       addr <= x"ab";
       data <= x"abcd";
       wait for 20 ns;
       cs <= '1';
       we <= '0';
       addr <= x"ae";
       data <= x"abcd";
       wait for 20 ns;
       wait;
   end process;
end behavioral; 
