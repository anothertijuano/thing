library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity PC_12 is 
     port(  
          INPUT : in  std_logic_vector(11 downto 0);
          OUTPUT: out std_logic_vector(11 downto 0);
          rw    : in  std_logic;
          up    : in  std_logic;
          e     : in  std_logic
     ); 
end PC_12;

architecture behavioral of PC_12 is
     signal q: std_logic_vector(11 downto 0):=x"000";
begin
     enable: process(e,q,rw,INPUT)
     begin
          if(e='0') then
               OUTPUT<=q;
          else
               OUTPUT<="ZZZZZZZZZZZZ";
          end if;
     end process;
     
     inc: process(q,up,rw)
     begin
          if(up'event and up='0') then
               q<=std_logic_vector(unsigned(unsigned(q) + 1));
          else 
               if(rw'event and rw='0') then
                    q<=INPUT;
               end if;
          end if;
     end process;

end behavioral;
