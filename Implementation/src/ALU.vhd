library ieee;
use ieee. std_logic_1164.all;
use ieee.numeric_std.all;


entity ALU is
     port( 
          e  : in std_logic;
          A  : in  std_logic_vector(7 downto 0);
          B  : in  std_logic_vector(7 downto 0);
          F  : in  std_logic_vector(3 downto 0);
          C  : out std_logic_vector(7 downto 0);
          q  : out std_logic_vector(3 downto 0)
     );
end ALU;

architecture behavioral of ALU is
     signal tmp: std_logic_vector(7 downto 0);
     signal tmp_q: std_logic_vector(3 downto 0);
begin
     enable: process(e,tmp)
     begin
          if(e='0') then
               C <= tmp;
          else
               C <= "ZZZZZZZZ";
          end if;
     end process;

     --Q3
     Zero_Flag: process(tmp)
     begin
          if(tmp=x"00") then
               q(3)<='1';
          else
               q(3)<='0';
          end if;
     end process;
     --Q2 ToDo

     --Q1
     q(1)<=tmp(7);
     
     --Q0
     q(0)<=tmp(0);
     
     --ALU operation
     with F select
          tmp <=
               A and B   when x"0",
               A or  B   when x"1",
               A xnor B  when x"2",
               A xor B   when x"3",
               std_logic_vector(unsigned(A) + unsigned(B))  when x"4",
               std_logic_vector(unsigned(A) - unsigned(B))  when x"5",
               std_logic_vector(signed(A) + signed(B))      when x"6",
               std_logic_vector(signed(A) - signed(B))      when x"7",
               A(6 downto  0) & '0'   when x"8",
               '0' & A(7 downto  1)   when x"9",
               A(6 downto  0) & A(7)  when x"A",
               A(0) & A(7 downto 1)   when others;
end behavioral;
