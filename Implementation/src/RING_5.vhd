library ieee;
use ieee. std_logic_1164.all;

entity RING is
     port(
          CLK  : in  std_logic;
          D    : in  std_logic;
          RESET: in  std_logic;
          Q    : out std_logic_vector(4 downto 0)
     );
end RING;

architecture behavioral of RING is
     signal q_tmp: std_logic_vector(4 downto 0):= "10000";
begin
     process(CLK,RESET)
     begin
          if(RESET='1') then
               q_tmp <= "10000";
          elsif Rising_edge(CLK) then
               q_tmp(1) <= q_tmp(0);
               q_tmp(2) <= q_tmp(1);
               q_tmp(3) <= q_tmp(2);
               q_tmp(4) <= q_tmp(3);
               q_tmp(0) <= q_tmp(4);
          end if;
     end process;
Q <= q_tmp;
end Behavioral;
