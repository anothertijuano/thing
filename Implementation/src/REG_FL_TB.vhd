library ieee;
use ieee.std_logic_1164.all;
entity REG_FL_TB is 
end REG_FL_TB;

architecture behavioral of REG_FL_TB is

     component REG_FL is 
     port( 
          rs1_a: in  std_logic_vector(3 downto 0);
          rs2_a: in  std_logic_vector(3 downto 0);
          rd_a : in  std_logic_vector(3 downto 0);
          rw   : in  std_logic;
          rs1_e: in  std_logic;
          rs2_e: in  std_logic;
          DEST : in  std_logic_vector(7 downto 0);
          S1   : out std_logic_vector(7 downto 0);
          S2   : out std_logic_vector(7 downto 0)
     ); 
     end component;
     
     signal rs1_a: std_logic_vector(3 downto 0);
     signal rs2_a: std_logic_vector(3 downto 0);
     signal rd_a : std_logic_vector(3 downto 0);
     signal rw   : std_logic;
     signal rs1_e: std_logic;
     signal rs2_e: std_logic;
     signal DEST : std_logic_vector(7 downto 0);
     signal S1   : std_logic_vector(7 downto 0);
     signal S2   : std_logic_vector(7 downto 0);

begin 
     uut: REG_FL port map(rs1_a,rs2_a,rd_a,rw,rs1_e,rs2_e,DEST,S1,S2);
     
     stim_proc: process
     begin
	     wait for 1 ns;
          rs1_a<=x"0";
          rs2_a<=x"0";
          rd_a<=x"0";
          rw<='1';
          rs1_e<='1';
          rs2_e<='1';
          DEST<=x"AA";
          wait for 20 ns;
          rw<='0';
          wait for 20 ns;
          rw<='1';
          rs1_a<=x"0";
          rs2_a<=x"0";
          rs1_e<='0';
          rs2_e<='0';
          wait for 20 ns;
          DEST<=x"55";
          rd_a<=x"1";
          rs1_e<='1';
          rs2_e<='1';
          wait for 20 ns;
          rw<='0';
          wait for 20 ns;
          rs1_a<=x"0";
          rs2_a<=x"1";
          rw<='1';
          wait for 20 ns;
          rs1_e<='0';
          rs2_e<='0';
          wait for 20 ns;
          rs1_e<='1';
          rs2_e<='1';
          wait for 20 ns;
       	wait;
     end process;
end behavioral; 
