library ieee;
use ieee.std_logic_1164.all;

entity BUFF_8 is 
     port(  
          input     :in  std_logic_vector(7 downto 0);
          output    :out std_logic_vector(7 downto 0);
          enable    :in  std_logic
     ); 
end BUFF_8;

architecture behavioral of BUFF_8 is
     component TRI_BUFF is
          port(
               input     : in  std_logic;
               output    : out std_logic;
               enable    : in  std_logic
          );
     end component;
begin 
     O_07: TRI_BUFF port map(input(7),  output(7),  enable);
     O_06: TRI_BUFF port map(input(6),  output(6),  enable);
     O_05: TRI_BUFF port map(input(5),  output(5),  enable);
     O_04: TRI_BUFF port map(input(4),  output(4),  enable);
     O_03: TRI_BUFF port map(input(3),  output(3),  enable);
     O_02: TRI_BUFF port map(input(2),  output(2),  enable);
     O_01: TRI_BUFF port map(input(1),  output(1),  enable);
     O_00: TRI_BUFF port map(input(0),  output(0),  enable);
end behavioral; 
