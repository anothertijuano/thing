library ieee;
use ieee.std_logic_1164.all;

entity PC_12_TB is 
end PC_12_TB;

architecture behavioral of PC_12_TB is
     
     component PC_12 is
          port(  
          INPUT : in  std_logic_vector(11 downto 0);
          OUTPUT: out std_logic_vector(11 downto 0);
          rw    : in  std_logic;
          up    : in  std_logic;
          e     : in  std_logic
          );
     end component;

     signal INPUT : std_logic_vector(11 downto 0);
     signal OUTPUT: std_logic_vector(11 downto 0);
     signal rw    : std_logic;
     signal up    : std_logic;
     signal e     : std_logic;

begin 
     uut: PC_12 port map(INPUT,OUTPUT,rw,up,e);
     
     stim_proc: process
     begin
	     wait for 20 ns;
	     INPUT<=x"AAA";
          rw<='1';
          up<='1';
          e<='1';
          wait for 20 ns;
          --Inc  
          up<='0';
          wait for 20 ns;
          up<='1';
          e<='0';
          wait for 20 ns;
          e<='1';
          wait for 20 ns;
          --Inc  
          up<='0';
          wait for 20 ns;
          up<='1';
          e<='0';
          wait for 20 ns;
          e<='1';
          wait for 20 ns;
          --Inc  
          up<='0';
          wait for 20 ns;
          up<='1';
          e<='0';
          wait for 20 ns;
          e<='1';
          wait for 20 ns;
          --Inc
          up<='0';
          wait for 20 ns;
          up<='1';
          e<='0';
          wait for 20 ns;
          e<='1';
          wait for 20 ns;
          --Inc
          up<='0';
          wait for 20 ns;
          up<='1';
          e<='0';
          wait for 20 ns;
          e<='1';
          wait for 20 ns;
          --Inc
          up<='0';
          wait for 20 ns;
          up<='1';
          e<='0';
          wait for 20 ns;
          e<='1';
          wait for 20 ns;
          --LD
          rw<='0';
          wait for 20 ns;
          e<='0';
          wait for 20 ns;
          e<='1';
          --Inc
          up<='0';
          wait for 20 ns;
          up<='1';
          e<='0';
          wait for 20 ns;
          e<='1';
          wait for 20 ns;
          --Inc
          up<='0';
          wait for 20 ns;
          up<='1';
          e<='0';
          wait for 20 ns;
          wait;
     end process;
end behavioral; 
