library ieee;
use ieee. std_logic_1164.all;
use ieee.numeric_std.all;
 
entity RAM is
     port( 
       cs : in std_logic;
       we : in std_logic;
       addr : in std_logic_vector(7 downto 0);
       data : inout std_logic_vector(15 downto 0)
     );
end RAM;

architecture behavioral of RAM is
  type memory_array_type is array (0 to 255) of std_logic_vector(15 downto 0);
  signal memory_array : memory_array_type;
begin
  
  process (cs, we)
  begin
    --write case (only case)
    if (cs='1' and we='1') then
      memory_array(to_integer(unsigned(addr))) <= data;
      data <= "ZZZZZZZZZZZZZZZZ";
    elsif (cs='1' and we='0') then
      data <= memory_array(to_integer(unsigned(addr)));
    else
      data <= "ZZZZZZZZZZZZZZZZ";
    end if;
  end process;
  
end behavioral;
